# multiAgents.py
# --------------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


from util import manhattanDistance
from game import Directions
import random, util

from game import Agent
from game import Actions
from pacman import PacmanRules
import math
from copy import copy
class ReflexAgent(Agent):
    """
      A reflex agent chooses an action at each choice point by examining
      its alternatives via a state evaluation function.

      The code below is provided as a guide.  You are welcome to change
      it in any way you see fit, so long as you don't touch our method
      headers.
    """


    def getAction(self, gameState):
        """
        You do not need to change this method, but you're welcome to.

        getAction chooses among the best options according to the evaluation function.

        Just like in the previous project, getAction takes a GameState and returns
        some Directions.X for some X in the set {North, South, West, East, Stop}
        """
        # Collect legal moves and successor states
        legalMoves = gameState.getLegalActions()

        # Choose one of the best actions
        scores = [self.evaluationFunction(gameState, action) for action in legalMoves]
        bestScore = max(scores)
        bestIndices = [index for index in range(len(scores)) if scores[index] == bestScore]
        chosenIndex = random.choice(bestIndices) # Pick randomly among the best

        "Add more of your code here if you want to"

        return legalMoves[chosenIndex]

    
    def evaluationFunction(self, currentGameState, action):
        """
        Design a better evaluation function here.

        The evaluation function takes in the current and proposed successor
        GameStates (pacman.py) and returns a number, where higher numbers are better.

        The code below extracts some useful information from the state, like the
        remaining food (newFood) and Pacman position after moving (newPos).
        newScaredTimes holds the number of moves that each ghost will remain
        scared because of Pacman having eaten a power pellet.

        Print out these variables to see what you're getting, then combine them
        to create a masterful evaluation function.
        """
        # Useful information you can extract from a GameState (pacman.py)
        successorGameState = currentGameState.generatePacmanSuccessor(action)
        newPos = successorGameState.getPacmanPosition()
        x, y = newPos
        newFood = successorGameState.getFood()
        consumables_list = copy(newFood.asList())
        if len(consumables_list) == 0:
            return successorGameState.getScore()
        walls = successorGameState.data.layout.walls
        w, h = walls.width, walls.height
        norm = lambda x: float(x)/(w+h)
        has_food = newFood[x][y]
        ghostPositions = successorGameState.getGhostPositions()
        mdists = [norm(manhattanDistance(newPos, newGPos)) for newGPos in ghostPositions]
        
        posx, posy = curpos = currentGameState.getPacmanPosition()        

        # Nearest food vector
        closest_food = lambda x: manhattanDistance(x, curpos)
        cfp = min(consumables_list, key=closest_food) # Closest food point
        nfv = (cfp[0]-posx, cfp[1]-posy)
        
        # Action direction vector
        adv = Actions.directionToVector( action, PacmanRules.PACMAN_SPEED )
        if action==Directions.STOP:
            return 0
        d1 = math.sqrt(nfv[0]*nfv[0] + nfv[1]*nfv[1])
        d2 = math.sqrt(adv[0]*adv[0] + adv[1]*adv[1])
        # print "nfv, adv = {0}, {1}".format(nfv, adv)
        # print " d1,  d2 = {0}, {1}".format(d1, d2)
        cos_theta = ((nfv[0]*adv[0] + nfv[1]*adv[1])/(d1*d2))
        return successorGameState.getScore() + sum(mdists)*(min(mdists)>norm(5)) + cos_theta #successorGameState.getScore() + 

def scoreEvaluationFunction(currentGameState):
    """
      This default evaluation function just returns the score of the state.
      The score is the same one displayed in the Pacman GUI.

      This evaluation function is meant for use with adversarial search agents
      (not reflex agents).
    """
    return currentGameState.getScore()

class MultiAgentSearchAgent(Agent):
    """
      This class provides some common elements to all of your
      multi-agent searchers.  Any methods defined here will be available
      to the MinimaxPacmanAgent, AlphaBetaPacmanAgent & ExpectimaxPacmanAgent.

      You *do not* need to make any changes here, but you can if you want to
      add functionality to all your adversarial search agents.  Please do not
      remove anything, however.

      Note: this is an abstract class: one that should not be instantiated.  It's
      only partially specified, and designed to be extended.  Agent (game.py)
      is another abstract class.
    """

    def __init__(self, evalFn = 'scoreEvaluationFunction', depth = '2'):
        self.index = 0 # Pacman is always agent index 0
        self.evaluationFunction = util.lookup(evalFn, globals())
        self.depth = int(depth)
        self.DEFAULT_ACTION = Directions.STOP
   
class MinimaxAgent(MultiAgentSearchAgent):
    """
      Your minimax agent (question 2)
    """

    def minimax(self, gameState, depth, agentIndex):
        numAgents = gameState.getNumAgents()
        if agentIndex == numAgents:
            depth -= 1
            agentIndex = 0
        if depth==0 or gameState.isWin() or gameState.isLose():
            return self.evaluationFunction(gameState)
        legalMoves = gameState.getLegalActions(agentIndex)
        if agentIndex==0: # Maximize
            best = -999999
            best_method = max
        else:
            best = 999999
            best_method = min
        for action in legalMoves:
            successorGameState = gameState.generateSuccessor(agentIndex, action)
            score = self.minimax(successorGameState, depth, agentIndex+1)
            best = best_method(score, best)
        return best

    def minimax_wrapper(self, gameState):
        best = (self.DEFAULT_ACTION, -999999)
        legalMoves = gameState.getLegalActions(0)
        for action in legalMoves:
            successorGameState = gameState.generateSuccessor(0, action)
            scored = (action, self.minimax(successorGameState, self.depth, 1))
            if scored[1]>best[1]:
                best = scored
        return best

    def getAction(self, gameState):
        """
          Returns the minimax action from the current gameState using self.depth
          and self.evaluationFunction.

          Here are some method calls that might be useful when implementing minimax.

          gameState.getLegalActions(agentIndex):
            Returns a list of legal actions for an agent
            agentIndex=0 means Pacman, ghosts are >= 1

          gameState.generateSuccessor(agentIndex, action):
            Returns the successor game state after an agent takes an action

          gameState.getNumAgents():
            Returns the total number of agents in the game
        """
        best_action_score = self.minimax_wrapper(gameState)
        return best_action_score[0]

class AlphaBetaAgent(MultiAgentSearchAgent):
    """
      Your minimax agent with alpha-beta pruning (question 3)
    """

    def minimax(self, gameState, depth, agentIndex, alpha, beta):
        numAgents = gameState.getNumAgents()
        if agentIndex == numAgents:
            depth -= 1
            agentIndex = 0
        if depth==0 or gameState.isWin() or gameState.isLose():
            return self.evaluationFunction(gameState)
        legalMoves = gameState.getLegalActions(agentIndex)
        if agentIndex==0: # Maximize
            best = -999999
            for action in legalMoves:
                successorGameState = gameState.generateSuccessor(agentIndex, action)
                score = self.minimax(successorGameState, depth, agentIndex+1, alpha, beta)
                best = max(score, best)
                alpha = max(alpha, best)
                if beta < alpha:
                    return best
        else:
            best = 999999
            for action in legalMoves:
                successorGameState = gameState.generateSuccessor(agentIndex, action)
                score = self.minimax(successorGameState, depth, agentIndex+1, alpha, beta)
                best = min(score, best)
                beta = min(beta, best)
                if beta < alpha:
                    return best
        return best

    def minimax_wrapper(self, gameState):
        best = (self.DEFAULT_ACTION, -999999)
        alpha = -999999
        beta = 999999
        legalMoves = gameState.getLegalActions(0)
        for action in legalMoves:
            successorGameState = gameState.generateSuccessor(0, action)
            scored = (action, self.minimax(successorGameState, self.depth, 1, alpha, beta))
            if scored[1]>best[1]:
                best = scored
            alpha = max(alpha, best[1])
            if beta < alpha:
                return best
        return best

    def getAction(self, gameState):
        """
          Returns the minimax action using self.depth and self.evaluationFunction
        """
        best_action_score = self.minimax_wrapper(gameState)
        return best_action_score[0]

class ExpectimaxAgent(MultiAgentSearchAgent):
    """
      Your expectimax agent (question 4)
    """

    def expectimax(self, gameState, depth, agentIndex):
        numAgents = gameState.getNumAgents()
        if agentIndex == numAgents:
            depth -= 1
            agentIndex = 0
        if depth==0 or gameState.isWin() or gameState.isLose():
            return self.evaluationFunction(gameState)
        legalMoves = gameState.getLegalActions(agentIndex)
        if agentIndex==0: # Maximize
            best = -999999
            for action in legalMoves:
                successorGameState = gameState.generateSuccessor(agentIndex, action)
                score = self.expectimax(successorGameState, depth, agentIndex+1)
                best = max(score, best)
            return best
        else:
            v = 0.0
            p = float(1)/len(legalMoves)
            for action in legalMoves:
                successorGameState = gameState.generateSuccessor(agentIndex, action)
                score = self.expectimax(successorGameState, depth, agentIndex+1)
                v += p*score
            return v

    def expectimax_wrapper(self, gameState):
        best = (self.DEFAULT_ACTION, -999999)
        legalMoves = gameState.getLegalActions(0)
        for action in legalMoves:
            successorGameState = gameState.generateSuccessor(0, action)
            scored = (action, self.expectimax(successorGameState, self.depth, 1))
            if scored[1]>best[1]:
                best = scored
        return best

    def getAction(self, gameState):
        """
          Returns the expectimax action using self.depth and self.evaluationFunction

          All ghosts should be modeled as choosing uniformly at random from their
          legal moves.
        """
        best_action_score = self.expectimax_wrapper(gameState)
        return best_action_score[0]

target_food = None
def isGoalFood(food, s):
    global target_food
    if target_food:
        if food[target_food[0]][target_food[1]]:
            return s==target_food
        else:
            target_food = None
            return isGoalFood(food, s)
    else:
        if food[s[0]][s[1]]:
            # Uncomment to fixate on a food item untill eaten
            # When commented, works like normally by just checking for food
            # target_food = s
            return True
        else:
            return False

# Reused from Homework-1
def bfs(currentGameState):
    visitCount = util.Counter()
    front = []
    food = currentGameState.getFood()
    walls = currentGameState.data.layout.walls
    s = currentGameState.getPacmanPosition()
    visitCount[s]=1
    if(isGoalFood(food, s)):
        return (s, [])
    front = [(currentGameState.generatePacmanSuccessor(action),action,1) for action in currentGameState.getLegalActions()]
    for i in range(0, len(front)):
        v = front[i]
        front[i] = (v[0], [v[1]], v[2])
    while(len(front)>0):
        newFront = []
        for v in front:
            [state, directions, cost] = v
            position = state.getPacmanPosition()
            if(isGoalFood(food, position)):
                return (position, directions)
            if(visitCount[position]>0):
                continue
            visitCount[position]+=1
            adj=[(state.generatePacmanSuccessor(action), action, cost+1) for action in state.getLegalActions(0)]
            for j in range(0, len(adj)):
                p = copy(v[1])
                p.append(adj[j][1])
                adj[j] = (adj[j][0], p, adj[j][2])
            newFront.extend(adj)
        front = newFront
    return (s,[])

def compute_path(currentGameState, actionList, formatter=lambda x: x):
    state = currentGameState
    s = [state.getPacmanPosition()]
    for action in actionList:
        if action in state.getLegalActions(0):
            nextState = state.generatePacmanSuccessor(action)
            s.append(nextState.getPacmanPosition())
            state = nextState
        else:
            s.append((-1,-1))
            break;
    return formatter(s) #" ==> ".join([str(x) for x in s])

prevpos = (-1, -1)
prev_path = None
prev_goal = None
def betterEvaluationFunction(currentGameState):
    """
      Your extreme ghost-hunting, pellet-nabbing, food-gobbling, unstoppable
      evaluation function (question 5).

      DESCRIPTION:  Return value is sum of 3 constants factor_a, factor_b,
                    factor_c, and getScore().

    """
    global prevpos
    global prev_path
    global prev_goal
    # Init general variables
    food = currentGameState.getFood()
    walls = currentGameState.data.layout.walls
    w, h = walls.width, walls.height
    posx, posy = current_position = currentGameState.getPacmanPosition()
    factor_a = 0
    factor_b = 0
    factor_c = 0

    """
        'consumables_list' was originally intended for Food/pellets array.
        Now used just to store list of foods. Returns score when no food found.
        (This scenario occurs sometimes)
    """
    consumables_list = copy(food.asList())
    if len(consumables_list) == 0:
        return currentGameState.getScore()

    # factor_a - Reciprocal of sum of manhattan distances to ghosts
    ghostPositions = currentGameState.getGhostPositions()
    mdists = [manhattanDistance(current_position, gpos) for gpos in ghostPositions]
    if sum(mdists) > 0:
        factor_a += 1.0/sum(mdists)

    # factor_b - Brownie-points if current position has food.
    if food[posx][posy]:
        factor_b += 1

    """
    factor_c - Reciprocal of path length to nearest food (by BFS) when the
               packman is stuck (current_position==prevpos).
               Packman could start moving and get stuck again. In order
               to fix this, the current position is checked against the
               previously computed path array. If exists, then the goal food
               is assumed to be unchanged, and prev_path is used to save
               computation and therefore speed up.
    """
    if current_position==prevpos:
        if prev_path is None or current_position not in prev_path or prev_goal not in consumables_list:
            goal, directions = bfs(currentGameState)
            path = compute_path(currentGameState, directions)
            prev_goal = goal
            prev_path = path
        else:
            goal = copy(prev_goal)
            path = copy(prev_path)
        path_length = len(path)
        path_length = 0.5 if path_length==0 else float(path_length)
        factor_c = 1.0/path_length
    else:
        prevpos = current_position

    net_value = factor_a + factor_b + factor_c + currentGameState.getScore()
    return net_value

# Abbreviation
better = betterEvaluationFunction

